import rq_dashboard
import rq_scheduler_dashboard
from flask import (
    Flask,
    Response,
    abort,
    redirect,
    render_template,
    request,
    session,
    url_for,
)
from flask_login import LoginManager, UserMixin, login_required, login_user, logout_user

SECRET_KEY = "why-is-a-white-space-called-white-space?-not-a-yellow-or-brown-space?"

app = Flask(__name__)
app.config.from_object(rq_scheduler_dashboard.default_settings)
app.config.update(
    DEBUG=False, SECRET_KEY=SECRET_KEY,
)

login_manager = LoginManager()
login_manager.init_app(app)
login_manager.login_view = "login"

app.register_blueprint(rq_dashboard.blueprint, url_prefix="/rq")
app.register_blueprint(
    rq_scheduler_dashboard.blueprint, url_prefix="/rq-scheduler-dashboard"
)


class User(UserMixin):
    pass


class NoAccessException(Exception):
    pass

@login_manager.unauthorized_handler
def unauthorized_handler():
    return """<h1>Oh no, You are unauthorized. Please login <a href="/login">here</a>"""


@login_manager.user_loader
def user_loader(email):
    user = User()
    user.id = email
    return user


@login_manager.request_loader
def request_loader(request):
    email = request.form.get('email')
    password = request.form.get("password")
    if not email or not password:
        raise NoAccessException()

    user = User()
    user.id = email

    user.is_authenticated = request.form['password'] == email + SECRET_KEY
    return user

@app.route("/")
@app.route("/dashboard")
@login_required
def dashboard():
    return """
           <a href="/rq">Click here</a> for rq dashboard.
           <br/>
           <a href="/rq-scheduler-dashboard">Click here</a> for rq-scheduler-dashboard.
           """


@app.route("/login", methods=["GET", "POST"])
def login():
    if request.method == "GET":
        return """
               <form action='login' method='POST'>
                <input type='text' name='email' id='email' placeholder='email'/>
                <input type='password' name='password' id='password' placeholder='password'/>
                <input type='submit' name='submit'/>
               </form>
               """

    email = request.form["email"]
    if request.form["password"] == email + SECRET_KEY:
        user = User()
        user.id = email
        login_user(user)
        return redirect(url_for("dashboard"))

    return "Bad login"


@app.errorhandler(NoAccessException)
def no_access(error):
    return redirect(url_for("login"))

if __name__ == "__main__":
    app.run()
